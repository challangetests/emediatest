<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Recipe;

class RecipeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('recipe/index', [
                'recipes' =>  Recipe::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('recipe/create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = $request->validate([
            'title' => 'required|string',
            'ingredients' => 'required|string',
            'preparation' => 'required|string'
        ]);
        $data = $request->all();
        
        Recipe::create([
            'title' => $data['title'],
            'ingredients' => $data['ingredients'],
            'preparation' => $data['preparation']
        ]);

        $request->session()->flash('status', 'Receita inserida com sucesso');
        return redirect()->route('recipes.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('recipe/edit', [
            'recipe' => Recipe::findOrFail($id)
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = $request->validate([
            'title' => 'required|string',
            'ingredients' => 'required|string',
            'preparation' => 'required|string'
        ]);
        $data = $request->all();

        Recipe::whereId($id)->update([
            'title' => $data['title'],
            'ingredients' => $data['ingredients'],
            'preparation' => $data['preparation']
        ]);

        $request->session()->flash('status', 'Receita alterada com sucesso');
        return redirect()->route('recipes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        Recipe::findOrFail($id)->delete();

        $request->session()->flash('status', 'Receita deletada com sucesso');
        return redirect()->route('recipes.index');
    }
}
