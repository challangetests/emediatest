@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Nova Receita</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form method="POST" action="/recipes">
                        @csrf   
                        <div class="form-group">
                            <label>Título:</label><br/>
                            <input type='text' name='title' placeholder="Título da receita" value="{{ old('title') }}" class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Ingredientes:</label><br/>
                            <textarea name='ingredients'  class="form-control" rows=3 >{{ old('ingredients') }} </textarea>  
                        </div>

                        <div class="form-group">
                            <label>Modo de preparo:</label><br/>
                            <textarea name='preparation'  class="form-control" rows=3 >{{ old('preparation') }} </textarea>
                         </div>   

                         <div class="form-group float-left">  
                            <a href="{{route('recipes.index')}}">
                                <button type="button" class="btn btn-secondary">Voltar</button>
                            </a>
                        </div>

                        <div class="form-group float-right">  
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
