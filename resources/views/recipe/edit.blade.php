@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> Editar Receita</div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    @php
                        $title = (old('title'))? old('title') : $recipe->title;
                        $ingredients = (old('ingredients'))? old('ingredients') : $recipe->ingredients;
                        $preparation = (old('preparation'))? old('preparation') : $recipe->preparation;
                    @endphp
                    <form method="POST" action="/recipes/{{$recipe->id}}">
                        @csrf   
                        @method('PUT')
                        <div class="form-group">
                            <label>Título:</label><br/>
                            <input type='text' name='title' placeholder="Título da receita" value="{{$title}}"
                                
                             class="form-control" />
                        </div>

                        <div class="form-group">
                            <label>Ingredientes:</label><br/>
                            <textarea name='ingredients'  class="form-control" rows=3 >{{ $ingredients }} </textarea>  
                        </div>

                        <div class="form-group">
                            <label>Modo de preparo:</label><br/>
                            <textarea name='preparation'  class="form-control" rows=3 >{{ $preparation }} </textarea>
                         </div>   

                         <div class="form-group float-left">  
                            <a href="{{route('recipes.index')}}">
                                <button type="button" class="btn btn-secondary">Voltar</button>
                            </a>
                        </div>

                        <div class="form-group float-right">  
                            <button type="submit" class="btn btn-primary">Cadastrar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
