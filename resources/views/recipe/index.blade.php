@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Home</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class='float-right form-group'>
                        <a href="{{route('recipes.create')}}">
                            <button type="button" class="btn btn-primary"> Nova Receita</button>
                        </a>                   
                    </div>

                    <table class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Titulo</th>
                            <th>Ingredientes</th>
                            <th>Modo de Preparo</th>
                            <th style='width:20%'></th>
                        </tr>
                    </thead>
                 <tbody>
                 @foreach($recipes as $recipe)
                    <tr>
                        <td>{{ $recipe->id }}</td>
                        <td>{{ $recipe->title }}</td>
                        <td>{{ $recipe->ingredients }}</td>
                        <td>{{ $recipe->preparation }}</td>
                        <td>
                            <form method="POST" action="/recipes/{{$recipe->id}}"
                            onsubmit="return confirm('Tem certeza que deseja deletar esta receita ?');">
                                @csrf   
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Deletar</button>
                                <a class="btn btn-small btn-secondary" href="{{ URL::to('recipes/' . $recipe->id . '/edit') }}">Editar</a>
                            </form>          
                        </td>
                    </tr>
                @endforeach
                 </tbody>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
